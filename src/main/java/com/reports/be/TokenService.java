package com.reports.be;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenService {
	
    private long EXPIRATIONTIME = 1000 * 60 * 60 * 24; // 1 day
    private String secret = "ThisIsASecret";
    
    public String addToken(String username) {
        // Generate a token.
        String jwt = Jwts.builder()
            .setSubject(username)
            .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
            .signWith(SignatureAlgorithm.HS512, secret)
            .compact();
        return jwt;
    }
    
    public boolean checkToken(String token) {
        if (token != null) {
			boolean tokenNoMoreValid = tokenIsInBlackList(token);
			if(tokenNoMoreValid) {
				throw new ExpiredJwtException(null, null, "Token no more available!");
			}
            // parse the token.
            String username = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
            if (username != null) // we managed to retrieve a user
            {
            	System.out.println("Inside check: token trusted");
                return true;
            }
        }
        System.out.println("Inside check: token not trusted");
        return false;
    }
    
    public Date getTokenExpirationDate(String token) {
        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody()
                .getExpiration();
    }
    
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    UserService userService;

	public String addTokenToBlackList(String token, String username) {
		if( !tokenIsInBlackList(token) ) {
			long userId = userService.findUserByUsername(username).getId();
			jdbcTemplate.update("INSERT INTO blacklisttoken VALUES (?, ?)", token, userId);
			return "Token added to black list!";
		} else {
			return "Token already in black list!";
		}
	}
    
    public boolean tokenIsInBlackList(String token) {
    	String sql = "SELECT count(*) FROM blacklisttoken WHERE token = ?";
    	boolean exists = false;
    	int count = jdbcTemplate.queryForObject(sql, new Object[] { token }, Integer.class);
    	exists = count > 0;
    	return exists;
    }
}
