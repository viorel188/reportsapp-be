package com.reports.be;

public class LoginResponse {
	private String username;
	private String token;
	private String message;
	private long id;
	
	public LoginResponse(String username, String token, String message, long id) {
		super();
		this.username = username;
		this.token = token;
		this.message = message;
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
}
