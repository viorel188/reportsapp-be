package com.reports.be;

public class ChangePasswordData {
	private String currentPassword;
	private String newPassword;
	private String newRePassword;
	
	public String getCurrentPassword() {
		return currentPassword;
	}
	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getNewRePassword() {
		return newRePassword;
	}
	public void setNewRePassword(String newRePassword) {
		this.newRePassword = newRePassword;
	}
	@Override
	public String toString() {
		return "ChangePasswordData [currentPassword=" + currentPassword + ", newPassword=" + newPassword
				+ ", newRePassword=" + newRePassword + "]";
	}
}
