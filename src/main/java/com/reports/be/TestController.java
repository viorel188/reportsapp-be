package com.reports.be;

import java.sql.SQLException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Controller
public class TestController {

	@Autowired
	UserService userService;

	@Autowired
	TokenService tokenService;

	@PostMapping(value = "/login2", consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<?> login(@RequestBody User user) {
		System.out.println("Login atempt for: " + user.getUsername() + " " + user.getPassword());
		try {
			User u = userService.findUserByUsername(user.getUsername());
			boolean doesMatch = BCrypt.checkpw(user.getPassword(), u.getPassword());
			if (doesMatch) {
				String token = tokenService.addToken(u.getUsername());
				LoginResponse loginRsp = new LoginResponse(u.getUsername(), token, "User logged in successfully!",
						u.getId());
				return new ResponseEntity<LoginResponse>(loginRsp, HttpStatus.OK);
			} else {
				// LoginResponse loginRsp = new LoginResponse(null,null,"Incorrect username or
				// password!");
				return new ResponseEntity<String>("Incorrect username or password!", HttpStatus.FORBIDDEN);
			}
		} catch (DataAccessException e) {
			// LoginResponse loginRsp = new LoginResponse(null,null,"(user doesn't exist)
			// Incorrect username or password!");
			return new ResponseEntity<String>("(user doesn't exist) Incorrect username or password!",
					HttpStatus.FORBIDDEN);
		}
	}

	@PostMapping(value = "/signup2", consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<?> signup(@RequestBody User user) {
		System.out.println(user.getUsername() + " " + user.getPassword());
		try {
			long id = userService.addUser(user);
			String token = tokenService.addToken(user.getUsername());
			LoginResponse loginRsp = new LoginResponse(user.getUsername(), token, "User logged in successfully!", id);
			return new ResponseEntity<LoginResponse>(loginRsp, HttpStatus.CREATED);
		} catch (SQLException e) {
			// LoginResponse loginRsp = new LoginResponse(null,null,"Username already
			// exists! Please, try a new one!");
			return new ResponseEntity<String>("Username already exists! Please, try a new one!",
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value = "/invalidateToken")
	ResponseEntity<?> invalidateToken(@RequestBody BlackListToken blToken) {
		Date tokenExpire = tokenService.getTokenExpirationDate(blToken.getToken());
		Date currentDate = new Date();
		if (tokenExpire.after(currentDate)) {
			String tokenStatus = tokenService.addTokenToBlackList(blToken.getToken(), blToken.getUsername());
			return new ResponseEntity<String>(tokenStatus, HttpStatus.CREATED);
		}
		return new ResponseEntity<String>("Token already invalidated!", HttpStatus.ALREADY_REPORTED);
	}

	@PutMapping(value = "/users/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<?> updateUserPassword(@PathVariable long id, @RequestBody ChangePasswordData changePasswordData,
			@RequestHeader("token") String token) {
		ResponseEntity<String> checkPasswordChangeReq = 
				validatePasswordChangeRequest(id, changePasswordData, token);
		if( checkPasswordChangeReq.getStatusCode()!=HttpStatus.OK ) {
			return checkPasswordChangeReq;
		}
		
		userService.updateUserPassword(id, changePasswordData.getNewPassword());
		System.out.println("TestController: id " + id);
		System.out.println("Cpd: " + changePasswordData);
		System.out.println("Tokkkken: " + token);
		
		return new ResponseEntity<String>("Password updated for user with id " + id, HttpStatus.OK);
	}

	private ResponseEntity<String> validatePasswordChangeRequest(long id, ChangePasswordData changePasswordData,
			String token) {
		try {
			tokenService.checkToken(token);
		} catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException
				| IllegalArgumentException e) {
			return new ResponseEntity<String>("Invalid token! Pls login!", HttpStatus.BAD_REQUEST);
		}
		if ( !changePasswordData.getNewPassword().equals(changePasswordData.getNewRePassword()) ) {
			return new ResponseEntity<String>("New password and repeated new password don't match!", HttpStatus.BAD_REQUEST);
		}
		User u = userService.findUserById(id);
		boolean doesMatch = BCrypt.checkpw(changePasswordData.getCurrentPassword(), u.getPassword());
		if (!doesMatch) {
			return new ResponseEntity<String>("Incorrect current password!", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>("ok!", HttpStatus.OK);
	}
}
