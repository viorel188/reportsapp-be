package com.reports.be.report;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ReportMapper implements RowMapper<Report>{

	@Override
	public Report mapRow(ResultSet rs, int rowNum) throws SQLException {
		Report report = new Report(
				rs.getLong("idReport"),
				rs.getString("type"),
				rs.getString("status"),
				rs.getTimestamp("created"),
				rs.getTimestamp("updated"),
				rs.getLong("idUser")
				);	
		return report;
	}

}
