package com.reports.be.report;

public enum ReportStatus {
	PENDING, DONE, FAILED
}
