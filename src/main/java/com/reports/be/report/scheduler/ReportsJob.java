package com.reports.be.report.scheduler;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.stereotype.Component;

import com.mysql.jdbc.Driver;
import com.reports.be.report.Report;
import com.reports.be.report.ReportService;

@Component
public class ReportsJob implements Job {
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println("In the job");

		SimpleDriverDataSource ds = new SimpleDriverDataSource();
        try {
			ds.setDriver(new Driver());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        ds.setUrl("jdbc:mysql://localhost:3306/reportappdb");
        ds.setUsername("root");
        ds.setPassword("root");

		JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);
		ReportService rs = new ReportService(jdbcTemplate);
		List<Report> reports = rs.getPendingReports();
		//System.out.println("reports:\n\n"+reports);
		
		File reportsDir = new File("reportsDir");
		if( !reportsDir.exists() ) {
			reportsDir.mkdir();
		}
		for(Report r : reports) {
			String reportFilename = r.getType() + "_" + r.getIdReport() + ".csv";
			generateCsvReport(reportsDir.getName() + File.separator + reportFilename);
		}

		
		rs.markAsDoneAllPendingReports();
		
		List<Report> rprts = rs.getReportsByUserId((long)19);
		System.out.println("User reports:\n" + rprts);
	}

	public void generateCsvReport(final String csvFileName) {
		List<String[]> dataLines = generateCsvLines();
	    File csvOutputFile = new File(csvFileName);
	    try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
	        dataLines.stream()
	          .map(this::convertToCSV)
	          .forEach(pw::println);
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
	}

	private List<String[]> generateCsvLines() {
		List<String[]> dataLines = new ArrayList<>();
		dataLines.add(new String[] { "RowId", "RowData" });
		int randomNrRows = randomNrOfRowsInclusive(4,9);
		for(int i=0;i<randomNrRows;++i) {
			dataLines.add(new String[] 
					  { (i+1)+"", "row data "+(i+1) });
		}
		return dataLines;
	}
	
	public String convertToCSV(String[] data) {
	    return Stream.of(data)
	      .map(this::escapeSpecialCharacters)
	      .collect(Collectors.joining(","));
	}
	
	public String escapeSpecialCharacters(String data) {
	    String escapedData = data.replaceAll("\\R", " ");
	    if (data.contains(",") || data.contains("\"") || data.contains("'")) {
	        data = data.replace("\"", "\"\"");
	        escapedData = "\"" + data + "\"";
	    }
	    return escapedData;
	}
	
	private int randomNrOfRowsInclusive(int min, int max) {
		Random rand = new Random();
		return rand.nextInt((max - min) + 1) + min;
	}
}
