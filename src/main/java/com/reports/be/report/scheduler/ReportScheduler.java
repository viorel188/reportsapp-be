package com.reports.be.report.scheduler;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class ReportScheduler {
	
	public void scanAndUpdateReports() {
		SchedulerFactory sf = new StdSchedulerFactory();
		try {
			Scheduler scheduler = sf.getScheduler();
			scheduler.start();
			
			JobDetail job = JobBuilder.newJob(ReportsJob.class)
					  .withIdentity("reportsJob")
					  .build();
			
			SimpleTrigger trigger = (SimpleTrigger) TriggerBuilder.newTrigger()
					  .withIdentity("reportsTrigger")
					  .startNow()
					  .withSchedule(SimpleScheduleBuilder.simpleSchedule()
							  .withIntervalInSeconds(60).repeatForever())
					  .build();
			
			scheduler.scheduleJob(job, trigger);
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
