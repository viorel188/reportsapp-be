package com.reports.be.report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class ReportService {

	JdbcTemplate jdbcTemplate;

	public ReportService(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public int addReport(int userId, String reportType) throws SQLException {
		
		final String INSERT_REPORT_SQL = "INSERT INTO report (type, status, created, updated, iduser) VALUES (?, ?, ?, ?, ?)";

		Connection connection = jdbcTemplate.getDataSource().getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(INSERT_REPORT_SQL,
				Statement.RETURN_GENERATED_KEYS);
		preparedStatement.setString(1, reportType);
		preparedStatement.setString(2, ReportStatus.PENDING.name());
		Timestamp currentTime = getCurrentTime();
		preparedStatement.setTimestamp(3, currentTime);
		preparedStatement.setTimestamp(4, currentTime);
		preparedStatement.setInt(5, userId);
		preparedStatement.executeUpdate();
		ResultSet keys = preparedStatement.getGeneratedKeys();

		if (keys.next()) {
			Integer generatedId = keys.getInt(1);
			return generatedId;
		}
		
		return -1;
	}
	
	public List<Report> getPendingReports() {
		String sql = "SELECT * FROM report WHERE status = '" + ReportStatus.PENDING.name() +"'";
		ReportMapper reportsMapper = new ReportMapper();
		List<Report> reports = jdbcTemplate.query(sql, reportsMapper);
		return reports;
	}
	
	public void markAsDoneAllPendingReports() {
		String updateSql = "UPDATE report SET status=?, updated=? WHERE status=?";
		jdbcTemplate.update(updateSql, ReportStatus.DONE.name(), getCurrentTime(), 
				ReportStatus.PENDING.name());
	}
	
	public List<Report> getReportsByUserId(long userId) {
		String sql = "SELECT * FROM report WHERE idUser = ?";
		List<Report> reports = jdbcTemplate.query(sql, new Object[] { userId }, new ReportMapper());
		return reports;
	}
	
	public void deleteReport(long reportId) {
		String deleteQuery = "delete from report where idReport = ?";
		jdbcTemplate.update(deleteQuery, reportId);
	}

	private java.sql.Timestamp getCurrentTime() {
		long timeNow = Calendar.getInstance().getTimeInMillis();
		java.sql.Timestamp ts = new java.sql.Timestamp(timeNow);
		return ts;
	}
}
