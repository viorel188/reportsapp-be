package com.reports.be.report;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.reports.be.TokenService;
import com.reports.be.UserService;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Controller
public class ReportGenerationController {

	@Autowired
	TokenService tokenService;

	@PostMapping(value = "/generateReport", consumes = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<String> generateReport(@RequestBody ReportRequest reportReq, @RequestHeader("token") String token) {
		System.out.println("Req: " + token);

		try {
			tokenService.checkToken(token);
			int userId = getUserId(reportReq.getUser());
			String reportType = reportReq.getReportType();
			boolean reportIsValid = contains(reportType);
			if (!reportIsValid) {
				return new ResponseEntity<String>("Incorrect report type requested!", HttpStatus.BAD_REQUEST);
			}
			int reportId = createReport(userId, reportType);
			return new ResponseEntity<String>("Report created with id: " + reportId, HttpStatus.CREATED);
		} catch (ExpiredJwtException e) {
			return new ResponseEntity<String>("Token expired! Please log in!", HttpStatus.BAD_REQUEST);
		} catch (UnsupportedJwtException | MalformedJwtException | SignatureException
				| IllegalArgumentException e) {
			return new ResponseEntity<String>("Token error! Please log in!", HttpStatus.BAD_REQUEST);
		} catch (SQLException e) {
			return new ResponseEntity<String>("An error occurred while requesting a report!",
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Autowired
	ReportService reportService;

	@GetMapping(value = "/getReportsForUser/{username}", produces = { MediaType.APPLICATION_JSON_VALUE })
	ResponseEntity<?> getReportsForUser(@PathVariable String username, @RequestHeader("token") String token) {
		try {
			tokenService.checkToken(token);
			long userId = userService.findUserByUsername(username).getId();
			List<Report> reports = reportService.getReportsByUserId(userId);
			Report[] reportsArr = new Report[reports.size()];
			reportsArr = reports.toArray(reportsArr);
			System.out.println("trusted!!!");
			return new ResponseEntity<Report[]>(reportsArr, HttpStatus.OK);
		} catch (ExpiredJwtException e) {
			return new ResponseEntity<String>("Token expired! Please log in!", HttpStatus.BAD_REQUEST);
		} catch (UnsupportedJwtException | MalformedJwtException | SignatureException
				| IllegalArgumentException e) {
			return new ResponseEntity<String>("Token error! Please log in!", HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping(value = "/reports/{id}")
	public ResponseEntity<String> deleteReport(@PathVariable("id") long id) 
	{
	    System.out.println("Report to be deleted: " + id);
	    reportService.deleteReport(id);
	    return new ResponseEntity<String>(HttpStatus.OK);
	}

	private int createReport(int userId, String reportType) throws SQLException {
		int reportId = reportService.addReport(userId, reportType);
		return reportId;
	}

	@Autowired
	UserService userService;

	private int getUserId(String user) {
		return userService.retrieveUserId(user);
	}

	private boolean contains(String reportType) {
		for (ReportType rt : ReportType.values()) {
			if (rt.name().equals(reportType)) {
				return true;
			}
		}
		return false;
	}
}
