package com.reports.be;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.reports.be.report.scheduler.ReportScheduler;

@SpringBootApplication
public class ReportAppBeApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(ReportAppBeApplication.class, args);
		
//		ReportScheduler rs = new ReportScheduler();
//		rs.scanAndUpdateReports();
	}

}
