package com.reports.be;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.reports.be.report.ReportStatus;

@Service
public class UserService {
	// @Autowired
	JdbcTemplate jdbcTemplate;

	public UserService(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public long addUser(User user) throws SQLException {
		final String INSERT_USER_SQL = "INSERT INTO user (username, password) VALUES (?, ?)";

		Connection connection = jdbcTemplate.getDataSource().getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER_SQL,
				Statement.RETURN_GENERATED_KEYS);
		preparedStatement.setString(1, user.getUsername());
		String cryptedPass = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());
		preparedStatement.setString(2, cryptedPass);

		preparedStatement.executeUpdate();
		ResultSet keys = preparedStatement.getGeneratedKeys();

		if (keys.next()) {
			Integer generatedId = keys.getInt(1); // id returned after insert execution
			return generatedId;
		}

		return -1;
	}
	
	public void updateUserPassword(long userId, String newPassword) {
		String cryptedNewPassword = BCrypt.hashpw(newPassword, BCrypt.gensalt());
		jdbcTemplate.update(
                "update user set password = ? where idUser = ?", 
                cryptedNewPassword, userId);
	}
	
    public User findUserById(Long id) {
        String sql = "SELECT * FROM user WHERE idUser = ?";
        User user = jdbcTemplate.queryForObject(sql, new Object[]{id}, new UserMapper());
        return user;
    }
    
    public User findUserByUsername(String username) {
        String sql = "SELECT * FROM user WHERE username = ?";
        User user = jdbcTemplate.queryForObject(sql, new Object[]{username}, new UserMapper());
        return user;
    }
    
    public int retrieveUserId(String username) {
        String sql = "SELECT idUser FROM user WHERE username = ?";
        int userId = jdbcTemplate.queryForObject(sql, new Object[]{username}, Integer.class);
        return userId;
    }
}
